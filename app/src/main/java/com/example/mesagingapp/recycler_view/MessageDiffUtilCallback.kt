package com.example.mesagingapp.recycler_view

import androidx.recyclerview.widget.DiffUtil
import com.example.mesagingapp.messages.DataMessage

class MessageDiffUtilCallback : DiffUtil.ItemCallback<DataMessage>() {
    override fun areItemsTheSame(oldItem: DataMessage, newItem: DataMessage): Boolean =
        oldItem.id == newItem.id

    override fun areContentsTheSame(oldItem: DataMessage, newItem: DataMessage): Boolean =
        oldItem == newItem
}