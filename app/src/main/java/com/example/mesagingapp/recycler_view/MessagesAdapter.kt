package com.example.mesagingapp.recycler_view

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.ListAdapter
import com.example.mesagingapp.R
import com.example.mesagingapp.messages.DataMessage
import com.example.mesagingapp.utils.GlideImageLoader

class MessagesAdapter(private val glideImageLoader: GlideImageLoader)
    : ListAdapter<DataMessage, DataMessageViewHolder>(MessageDiffUtilCallback()) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DataMessageViewHolder {
        val messageView = LayoutInflater
            .from(parent.context)
            .inflate(R.layout.message_item, parent, false)

        val messageViewHolder = DataMessageViewHolder(messageView)
        return messageViewHolder
    }

    override fun onBindViewHolder(holder: DataMessageViewHolder, position: Int) {
        val message = currentList[position]
        holder.messageContent.text = message.message
        holder.title.text = message.title
        glideImageLoader.load(holder.icon, message.iconUrl)
    }
}