package com.example.mesagingapp.recycler_view

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.example.mesagingapp.databinding.MessageItemBinding

class DataMessageViewHolder(private val view: View) : RecyclerView.ViewHolder(view) {
    private val binding = MessageItemBinding.bind(view)

    val icon get() = binding.icon
    val title get() = binding.title
    val messageContent get() = binding.messageContent
}