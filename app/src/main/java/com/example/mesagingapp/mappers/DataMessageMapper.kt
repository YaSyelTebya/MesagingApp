package com.example.mesagingapp.mappers

import android.os.Bundle
import com.example.mesagingapp.firebase.ICON_KEY
import com.example.mesagingapp.firebase.MESSAGE_CONTENT_KEY
import com.example.mesagingapp.firebase.TITLE_KEY
import com.example.mesagingapp.messages.DataMessage
import javax.inject.Inject

class DataMessageMapper @Inject constructor() {
    fun map(messageData: Bundle) = DataMessage(
        messageData.getString(ICON_KEY)!!,
        messageData.getString(TITLE_KEY)!!,
        messageData.getString(MESSAGE_CONTENT_KEY)!!
    )

    fun map(messageData: Map<String, String>) = DataMessage(
        messageData[ICON_KEY]!!,
        messageData[TITLE_KEY]!!,
        messageData[MESSAGE_CONTENT_KEY]!!
    )
}
