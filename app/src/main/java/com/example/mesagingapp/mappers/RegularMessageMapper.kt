package com.example.mesagingapp.mappers

import android.os.Bundle
import com.example.mesagingapp.firebase.MESSAGE_CONTENT_KEY
import com.example.mesagingapp.firebase.SENDER_KEY
import com.example.mesagingapp.messages.RegularMessage

class RegularMessageMapper {
    fun map(messageData: Bundle): RegularMessage {
        val sender = checkNotNull(messageData.getString(SENDER_KEY))
        val message = checkNotNull(messageData.getString(MESSAGE_CONTENT_KEY))
        return RegularMessage(sender, message)
    }
}