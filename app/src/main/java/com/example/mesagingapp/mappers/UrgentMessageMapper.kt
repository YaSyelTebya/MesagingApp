package com.example.mesagingapp.mappers

import android.os.Bundle
import com.example.mesagingapp.firebase.EXPIRATION_DATE_KEY
import com.example.mesagingapp.firebase.MESSAGE_CONTENT_KEY
import com.example.mesagingapp.firebase.SENDER_KEY
import com.example.mesagingapp.messages.UrgentMessage

class UrgentMessageMapper {
    fun map(messageData: Bundle): UrgentMessage {
        val sender = checkNotNull(messageData.getString(SENDER_KEY))
        val message = checkNotNull(messageData.getString(MESSAGE_CONTENT_KEY))
        val expirationDate = checkNotNull(messageData.getString(EXPIRATION_DATE_KEY))
        return UrgentMessage(sender, message, expirationDate)
    }
}