package com.example.mesagingapp

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.mesagingapp.db.MessageDao
import com.example.mesagingapp.messages.DataMessage
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class MessagesViewModel @Inject constructor(private val databaseDataSource: MessageDao) :
    ViewModel() {
    private val _messages = MutableLiveData(emptyList<DataMessage>())
    val messages: LiveData<List<DataMessage>> = _messages

    fun loadMessages() {
        viewModelScope.launch {
            databaseDataSource.getAllMessages().collect {
                _messages.postValue(it)
            }
        }
    }

    fun saveMessage(dataMessage: DataMessage) {
        viewModelScope.launch {
            databaseDataSource.addMessage(dataMessage)
        }
    }
}