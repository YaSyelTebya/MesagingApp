package com.example.mesagingapp.di.modules

import android.content.Context
import com.example.mesagingapp.db.MessageDao
import com.example.mesagingapp.db.MessagesDatabase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
class RoomModule {
    @Provides
    fun provideMessagesDatabase(
        @ApplicationContext context: Context
    ): MessagesDatabase =
        MessagesDatabase.getDatabase(context)

    @Provides
    fun provideMessagesDao(messagesDatabase: MessagesDatabase): MessageDao =
        messagesDatabase.messageDao()
}