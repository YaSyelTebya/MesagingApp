package com.example.mesagingapp

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.mesagingapp.databinding.ActivityRegularMessageDetailBinding
import com.example.mesagingapp.firebase.MESSAGE_CONTENT_KEY
import com.example.mesagingapp.firebase.SENDER_KEY

class RegularMessageDetailActivity : AppCompatActivity() {
    private lateinit var binding: ActivityRegularMessageDetailBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityRegularMessageDetailBinding.inflate(layoutInflater)
        setContentView(binding.root)
    }

    override fun onStart() {
        super.onStart()
        val message: Bundle = intent.extras!!

        val sender = message.getString(SENDER_KEY)!!
        val messageContent = message.getString(MESSAGE_CONTENT_KEY)!!

        showMessage(sender, messageContent)
    }

    private fun showMessage(sender: String, messageContent: String) {
        binding.sender.text = getString(R.string.sender_textview_template, sender)
        binding.message.text = messageContent
    }
}