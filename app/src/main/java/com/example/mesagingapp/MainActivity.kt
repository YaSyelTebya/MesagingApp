package com.example.mesagingapp

import android.Manifest
import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.core.os.bundleOf
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.mesagingapp.databinding.ActivityMainBinding
import com.example.mesagingapp.firebase.EXPIRATION_DATE_KEY
import com.example.mesagingapp.firebase.MESSAGE_CONTENT_KEY
import com.example.mesagingapp.firebase.MESSAGE_TYPE_KEY
import com.example.mesagingapp.firebase.MessageType
import com.example.mesagingapp.firebase.SENDER_KEY
import com.example.mesagingapp.mappers.DataMessageMapper
import com.example.mesagingapp.mappers.RegularMessageMapper
import com.example.mesagingapp.mappers.UrgentMessageMapper
import com.example.mesagingapp.messages.DataMessage
import com.example.mesagingapp.messages.RegularMessage
import com.example.mesagingapp.messages.UrgentMessage
import com.example.mesagingapp.recycler_view.MessagesAdapter
import com.example.mesagingapp.utils.GlideImageLoader
import com.example.mesagingapp.utils.MessageTypeDefiner
import com.google.firebase.messaging.FirebaseMessaging
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    @Inject
    lateinit var glideImageLoader: GlideImageLoader

    @Inject
    lateinit var messageTypeDefiner: MessageTypeDefiner

    private val viewModel: MessagesViewModel by viewModels()

    private lateinit var binding: ActivityMainBinding
    private val requestPostNotificationPermissionLauncher =
        registerForActivityResult(ActivityResultContracts.RequestPermission()) { isGranted ->
            if (isGranted) {
                showInLongToast("Permission granted")
            } else {
                showInLongToast("Permission denied")
            }
        }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)

        setContentView(binding.root)

        viewModel.loadMessages()

        askPermissionPostNotifications()

        setupRecyclerView()

        createUrgentNotificationChannel()

        createRegularNotificationChannel()

        val notificationExtras = intent.extras

        notificationExtras ?: return

        openMessage(notificationExtras)
    }

    private fun setupRecyclerView() {
        binding.messagesList.layoutManager = LinearLayoutManager(this)

        binding.messagesList.adapter = MessagesAdapter(glideImageLoader)

        viewModel.messages.observe(this) { messages ->
            (binding.messagesList.adapter as MessagesAdapter).submitList(messages)
        }
    }

    private fun openMessage(messageData: Bundle) {
        try {
            performMessage(messageData)
        } catch (e: IllegalArgumentException) {
            showInLongToast("Illegal message data provided ${e.message}")
        }
    }

    override fun onStart() {
        super.onStart()

        FirebaseMessaging.getInstance().token.addOnCompleteListener { task ->
            if (!task.isSuccessful) {
                Log.w(TAG, "Fetching FCM registration token failed", task.exception)
                return@addOnCompleteListener
            }
            val token = task.result

            Log.d(TAG, token)
        }
    }

    private fun createUrgentNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

            val name = getString(R.string.urgent_channel_name)
            val descriptionText = getString(R.string.urgent_channel_description)
            val importance = NotificationManager.IMPORTANCE_DEFAULT
            val urgentChannel =
                NotificationChannel(URGENT_NOTIFICATIONS_CHANNEL_ID, name, importance)
            urgentChannel.description = descriptionText
            val notificationManager = getSystemService(NOTIFICATION_SERVICE) as NotificationManager
            notificationManager.createNotificationChannel(urgentChannel)
        }
    }

    private fun createRegularNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

            val name = getString(R.string.regular_channel_name)
            val descriptionText = getString(R.string.regular_channel_description)
            val importance = NotificationManager.IMPORTANCE_LOW
            val regularChannel =
                NotificationChannel(REGULAR_NOTIFICATIONS_CHANNEL_ID, name, importance)
            regularChannel.description = descriptionText
            val notificationManager = getSystemService(NOTIFICATION_SERVICE) as NotificationManager
            notificationManager.createNotificationChannel(regularChannel)
        }
    }

    private fun askPermissionPostNotifications() {
        when {
            ContextCompat.checkSelfPermission(
                this,
                Manifest.permission.POST_NOTIFICATIONS
            ) == PackageManager.PERMISSION_DENIED -> {
                requestPostNotificationPermissionLauncher
                    .launch(Manifest.permission.POST_NOTIFICATIONS)
            }
        }
    }

    private fun showInLongToast(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show()
    }

    private fun performMessage(messageData: Bundle) {
        val rawMessageType = requireNotNull(messageData.getString(MESSAGE_TYPE_KEY))
        val messageType = getMessageType(rawMessageType)

        when (messageType) {
            MessageType.URGENT -> showUrgentMessage(messageData.mapToUrgent())
            MessageType.REGULAR -> showRegularMessage(messageData.mapToRegular())
            MessageType.DATA -> saveDataMessage(messageData.mapToData())
        }
    }

    private fun saveDataMessage(dataMessage: DataMessage) =
        viewModel.saveMessage(dataMessage)

    private fun getMessageType(rawMessageType: String): MessageType =
        messageTypeDefiner.getMessageType(rawMessageType)

    private fun showRegularMessage(message: RegularMessage) {
        val regularActivityIntent = Intent(this, RegularMessageDetailActivity::class.java)
        val regularMessageExtras = bundleOf(
            SENDER_KEY to message.sender,
            MESSAGE_CONTENT_KEY to message.message
        )

        launchActivityWithExtras(regularActivityIntent, regularMessageExtras)
    }

    private fun showUrgentMessage(message: UrgentMessage) {
        val urgentActivityIntent = Intent(this, UrgentMessageDetailActivity::class.java)
        val urgentMessageExtras = bundleOf(
            SENDER_KEY to message.sender,
            MESSAGE_CONTENT_KEY to message.message,
            EXPIRATION_DATE_KEY to message.expirationDateTime
        )

        launchActivityWithExtras(urgentActivityIntent, urgentMessageExtras)
    }

    private fun launchActivityWithExtras(intent: Intent, extras: Bundle) {
        intent.putExtras(extras)

        startActivity(intent)
    }

    private fun Bundle.mapToRegular(): RegularMessage =
        RegularMessageMapper()
            .map(this)

    private fun Bundle.mapToUrgent(): UrgentMessage =
        UrgentMessageMapper()
            .map(this)

    private fun Bundle.mapToData(): DataMessage =
        DataMessageMapper()
            .map(this)

    companion object {
        private const val TAG = "MessagingAppMainActivity"
        const val URGENT_NOTIFICATIONS_CHANNEL_ID = "URGENT_NOTIFICATIONS_CHANNEL"
        const val REGULAR_NOTIFICATIONS_CHANNEL_ID = "REGULAR_NOTIFICATIONS_CHANNEL"
    }
}