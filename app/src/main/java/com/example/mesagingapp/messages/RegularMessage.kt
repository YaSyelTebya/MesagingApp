package com.example.mesagingapp.messages

data class RegularMessage(
    val sender: String,
    val message: String
)
