package com.example.mesagingapp.messages

data class UrgentMessage(
    val sender: String,
    val message: String,
    val expirationDateTime: String
)
