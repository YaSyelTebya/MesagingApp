package com.example.mesagingapp.messages

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "messages")
data class DataMessage(
    @ColumnInfo(name = "icon_url") val iconUrl: String,
    @ColumnInfo(name = "title") val title: String,
    @ColumnInfo(name = "message") val message: String,
    @PrimaryKey(autoGenerate = true) val id: Int = 0
)
