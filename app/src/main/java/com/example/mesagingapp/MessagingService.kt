package com.example.mesagingapp

import android.annotation.SuppressLint
import com.example.mesagingapp.db.MessageDao
import com.example.mesagingapp.firebase.MESSAGE_TYPE_KEY
import com.example.mesagingapp.firebase.MessageType
import com.example.mesagingapp.mappers.DataMessageMapper
import com.example.mesagingapp.messages.DataMessage
import com.example.mesagingapp.utils.MessageTypeDefiner
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@SuppressLint("MissingFirebaseInstanceTokenRefresh")
@AndroidEntryPoint
class MessagingService : FirebaseMessagingService() {
    @Inject
    lateinit var databaseDataSource: MessageDao

    @Inject
    lateinit var dataMessageMapper: DataMessageMapper

    @Inject
    lateinit var messageTypeDefiner: MessageTypeDefiner

    override fun onMessageReceived(message: RemoteMessage) {
        val messageType = messageTypeDefiner.getMessageType(message.data[MESSAGE_TYPE_KEY]!!)
        if (messageType == MessageType.DATA) {
            val dataMessage = dataMessageMapper.map(message.data)
            saveDataMessage(dataMessage)
        }
        super.onMessageReceived(message)
    }

    private fun saveDataMessage(message: DataMessage) {
        databaseDataSource.addMessage(message)
    }
}