package com.example.mesagingapp.utils

import android.content.Context
import android.widget.ImageView
import com.bumptech.glide.Glide
import dagger.hilt.android.qualifiers.ApplicationContext
import javax.inject.Inject

class GlideImageLoader @Inject constructor(@ApplicationContext private val context: Context) {
    fun load(into: ImageView, url: String) {
        Glide.with(context)
            .load(url)
            .centerCrop()
            .override(540, 540)
            .into(into)
    }
}