package com.example.mesagingapp.utils

import com.example.mesagingapp.firebase.DATA_MESSAGE
import com.example.mesagingapp.firebase.MessageType
import com.example.mesagingapp.firebase.REGULAR_MESSAGE
import com.example.mesagingapp.firebase.URGENT_MESSAGE
import javax.inject.Inject

class MessageTypeDefiner @Inject constructor() {
    fun getMessageType(rawMessageType: String): MessageType = when (rawMessageType) {
        URGENT_MESSAGE -> MessageType.URGENT
        REGULAR_MESSAGE -> MessageType.REGULAR
        DATA_MESSAGE -> MessageType.DATA
        else -> error("Unsupported message type")
    }
}