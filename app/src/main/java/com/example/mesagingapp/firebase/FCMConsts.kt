package com.example.mesagingapp.firebase

const val SENDER_KEY = "sender"
const val ICON_KEY = "icon"
const val TITLE_KEY = "title"
const val MESSAGE_CONTENT_KEY = "message_content"
const val MESSAGE_TYPE_KEY = "type"
const val URGENT_MESSAGE = "urgent_message"
const val REGULAR_MESSAGE = "regular_message"
const val DATA_MESSAGE = "data_message"
const val EXPIRATION_DATE_KEY = "expiration_date"