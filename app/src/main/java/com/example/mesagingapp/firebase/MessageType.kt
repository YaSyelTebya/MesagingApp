package com.example.mesagingapp.firebase

enum class MessageType {
    URGENT, REGULAR, DATA
}