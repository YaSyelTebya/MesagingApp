package com.example.mesagingapp.db

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.mesagingapp.messages.DataMessage
import kotlinx.coroutines.flow.Flow

@Dao
interface MessageDao {
    @Query("SELECT * FROM messages")
    fun getAllMessages(): Flow<List<DataMessage>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun addMessage(message: DataMessage)
}