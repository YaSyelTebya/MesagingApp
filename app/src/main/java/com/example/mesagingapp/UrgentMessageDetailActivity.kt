package com.example.mesagingapp

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.mesagingapp.databinding.ActivityUrgentMessageDetailBinding
import com.example.mesagingapp.firebase.EXPIRATION_DATE_KEY
import com.example.mesagingapp.firebase.MESSAGE_CONTENT_KEY
import com.example.mesagingapp.firebase.SENDER_KEY

class UrgentMessageDetailActivity : AppCompatActivity() {
    private lateinit var binding: ActivityUrgentMessageDetailBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityUrgentMessageDetailBinding.inflate(layoutInflater)
        setContentView(binding.root)
    }

    override fun onStart() {
        super.onStart()
        val message: Bundle = intent.extras!!

        val sender = message.getString(SENDER_KEY)!!
        val messageContent = message.getString(MESSAGE_CONTENT_KEY)!!
        val expireDate = message.getString(EXPIRATION_DATE_KEY)!!

        showMessage(sender, messageContent, expireDate)
    }

    private fun showMessage(sender: String, messageContent: String, expireDate: String) {
        binding.sender.text = getString(R.string.sender_textview_template, sender)
        binding.message.text = messageContent
        binding.expireDate.text = expireDate
    }
}